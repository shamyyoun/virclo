package com.example.pc.khaledwaleedshopping.Support.webservice;

/**
 * Created by pc on 8/26/2017.
 */

public class WebService {

//    public static final String END_POINT = "http://192.168.1.9/virclo/";
    public static final String END_POINT = "http://mahmoudelshamy.com/virclo-api/";
//    public static final String APIS_END_POINT = END_POINT + "webservice/";
    public static final String APIS_END_POINT = END_POINT;
    public static String clientProducts = APIS_END_POINT + "productsByClientId.php?id_client=";
    public static String similarProducts = APIS_END_POINT + "productsByBrandIdLimit.php?";
    public static String productProfileData = APIS_END_POINT + "productProfileData.php?";
    public static String addProductData = APIS_END_POINT + "productContentData.php";
    public static String signUp = APIS_END_POINT + "sinup.php?";
    public static String forgetPassword = APIS_END_POINT + "forgetpassword.php?email=";
    public static String login = APIS_END_POINT + "login.php?";
    public static String addProduct = APIS_END_POINT + "addProduct.php?";
    public static String getAllProducts = APIS_END_POINT + "productsLimit.php?limit=";
    public static String imageLink = END_POINT + "shopping/public/website/product_images/";
    public static String updateView = APIS_END_POINT + "addProductView.php?id_product=";
    public static String updateLove = APIS_END_POINT + "addOrRemoveFavoutate.php?";
    public static String getComments = APIS_END_POINT + "productComments.php?id_product=";
    public static String postComments = APIS_END_POINT + "addComment.php?";
    public static String favourites = APIS_END_POINT + "productClientLove.php?limit=0&id_client=";
    public static String editProductData = APIS_END_POINT + "editProductData.php?id=";
    public static String updateProduct = APIS_END_POINT + "updateProduct.php?";
    public static String deleteProduct = APIS_END_POINT + "deleteProduct.php?id=";
    public static String userInfo = APIS_END_POINT + "editClientData.php?id=";
    public static String clientData = APIS_END_POINT + "clientData.php?";
    public static String updateClient = APIS_END_POINT + "updateClient.php?";
    public static String deleteComment = APIS_END_POINT + "deleteComment.php?id=";
    public static String updateFollow = APIS_END_POINT + "addFollow.php?";
    public static String postReview = APIS_END_POINT + "addReview.php?";
    public static String getReviews = APIS_END_POINT + "clientReviewData.php?id_client=";
    public static String deleteReview = APIS_END_POINT + "deleteReview.php?id=";
    public static String getFollowing = APIS_END_POINT + "clientFollowing.php?";
    public static String getFollowers = APIS_END_POINT + "clientFollowers.php?";
    public static String updateRegId = APIS_END_POINT + "clientRegId.php?";
    public static String sendMsg = APIS_END_POINT + "addMessage.php?";
    public static String getChat = APIS_END_POINT + "clientMessages.php?";
    public static String seeMsg = APIS_END_POINT + "seeMessage.php?";
    public static String filter=APIS_END_POINT + "productAllSearch.php?limit=";
    public static String search=APIS_END_POINT + "productSearch.php?limit=";
    public static String inbox=APIS_END_POINT + "clientChatList.php?id=";
    public static String getForums=APIS_END_POINT + "formsAll.php?id_kind=";
    public static String getForumsKinds=APIS_END_POINT + "allFormsKind.php";
    public static String getForumsProfile=APIS_END_POINT + "formsContentById.php?id=";
    public static String postCommentsForum=APIS_END_POINT + "addFormsComment.php?";
    public static String deleteCommentForum=APIS_END_POINT + "deleteFormsComment.php?id=";
    public static String getCommentsForum=APIS_END_POINT + "formsComments.php?id_form=";
}
